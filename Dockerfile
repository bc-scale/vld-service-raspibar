FROM node:18.7-alpine as development

ENV PUPPETEER_SKIP_DOWNLOAD true

WORKDIR /usr/src/app

RUN apk add --no-cache python3 g++ make

COPY package*.json tsconfig*.json ./

RUN npm i

COPY . .

RUN npm run build

RUN npm prune --production

FROM node:18.7-alpine

ENV PUPPETEER_SKIP_DOWNLOAD true

WORKDIR /usr/src/app

RUN apk add --no-cache python3 g++ make

COPY . .

COPY --from=development /usr/src/app/dist ./dist
COPY --from=development /usr/src/app/node_modules ./node_modules

CMD ["node", "dist/main"]
