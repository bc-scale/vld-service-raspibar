import { hostname } from 'os';

const {
  name,
  version,
} = require('../../package.json');

export default () => ({
  db: {
    uri: process.env.FIREBASE_CONFIG_URL,
  },
  server: {
    port: process.env.PORT || '3000',
  },
  service: {
    version,
    name: name || 'service-raspibar',
    hostname: hostname(),
    startedAt: new Date(),
  },
});
