import { plainToClass } from 'class-transformer';
import { IsNotEmpty, IsString, validateSync } from 'class-validator';

class EnvironmentVariables {
  @IsString()
  @IsNotEmpty()
    FIREBASE_CONFIG_URL: string;
}

export default function validate(config: Record<string, unknown>) {
  const finalConfig = plainToClass(
    EnvironmentVariables,
    config,
    { enableImplicitConversion: false },
  );

  const errors = validateSync(finalConfig, { skipMissingProperties: false });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }

  return finalConfig;
}
