import { IPin } from './IPin';

export default class IRaspberryPi {
  host: string;

  ip: string;

  status: string;

  gpios: IPin[];
}
