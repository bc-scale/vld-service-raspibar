export interface IPump {
  pump_id: string;
  pump_name: string;
  ingredient_id: string;
  pin_id: string;
}
