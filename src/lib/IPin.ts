export interface IPin {
  pin_bcm: string;
  pin_gpio: string;
  pin_id: string;
  pin_name: string;
}
