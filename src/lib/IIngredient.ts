export interface IIngredient {
  ingredient_id: string;
  ingredient_name: string;
  ingredient_img: string;
}
