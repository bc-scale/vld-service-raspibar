import { IIngredient } from './IIngredient';

export interface IRecipe {
  recipe_id: string;
  recipe_name: string;
  ingredients: IIngredient[];
}
