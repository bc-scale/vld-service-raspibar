import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import AppModule from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(app.get(ConfigService).get<string>('server.port'), '0.0.0.0');
}
bootstrap();
