import { Gpio } from 'onoff';
import { Injectable } from "@nestjs/common";
/* eslint-disable class-methods-use-this */
@Injectable()
export default class AppService {
  getHello(): string {
    const gpio = new Gpio(17, 'out');
    gpio.write(1);
    setTimeout(()=> {
      gpio.write(0);
    }, 5000);

    return "Hello World!";
  }
}
/* eslint-enable class-methods-use-this */
