const Persons = [
  {
    id: 'person_1',
    name: 'Vincent Leclerc',
  },
  {
    id: 'person_2',
    name: 'Jean Dupont',
  },
];

module.exports = Persons;
