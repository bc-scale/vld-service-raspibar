const Pumps = [
  {
    id: 'pump_1',
    name: 'Pompe 1',
    gpio: '01',
    ingredient: 'vodka_id', // modify ingredient here
  },
  {
    id: 'pump_2',
    name: 'Pompe 2',
    gpio: '02',
    ingredient: 'vodka_id',
  },
  {
    id: 'pump_3',
    name: 'Pompe 3',
    gpio: '03',
    ingredient: 'vodka_id',
  },
  {
    id: 'pump_4',
    name: 'Pompe 4',
    gpio: '04',
    ingredient: 'vodka_id',
  },
  {
    id: 'pump_5',
    name: 'Pompe 5',
    gpio: '05',
    ingredient: 'vodka_id',
  },
  {
    id: 'pump_6',
    name: 'Pompe 6',
    gpio: '06',
    ingredient: 'vodka_id',
  },
  {
    id: 'pump_7',
    name: 'Pompe 7',
    gpio: '07',
    ingredient: 'vodka_id',
  },
  {
    id: 'pump_8',
    name: 'Pompe 8',
    gpio: '08',
    ingredient: 'vodka_id',
  },
];

module.exports = Pumps;
