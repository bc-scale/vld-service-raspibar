const Recettes = [
  {
    id: 'recette1',
    name: 'Wisky Coca',
    author_id: 'person_1',
    composition: [
      {
        ingredient_id: 'wisky_id',
        ingredient_qty: '5',
        ingredient_unit: 'cl',
      },
      {
        ingredient_id: 'coca_id',
        ingredient_qty: '7',
        ingredient_unit: 'cl',
      },
    ],
  },
  {
    id: 'recette2',
    name: 'Skrew Driver',
    author_id: 'person_1',
    composition: [
      {
        ingredient_id: 'vodka_id',
        ingredient_qty: '4',
        ingredient_unit: 'cl',
      },
      {
        ingredient_id: 'orange_id',
        ingredient_qty: '12',
        ingredient_unit: 'cl',
      },
    ],
  },
  {
    id: 'recette3',
    name: 'Skrew Driver Hard',
    author_id: 'person_2',
    composition: [
      {
        ingredient_id: 'vodka_id',
        ingredient_qty: '6',
        ingredient_unit: 'cl',
      },
      {
        ingredient_id: 'orange_id',
        ingredient_qty: '12',
        ingredient_unit: 'cl',
      },
    ],
  },
  {
    id: 'recette4',
    name: 'Longiceland',
    author_id: 'person_1',
    composition: [
      {
        ingredient_id: 'gin_id',
        ingredient_qty: '3',
        ingredient_unit: 'cl',
      },
      {
        ingredient_id: 'vodka_id',
        ingredient_qty: '3',
        ingredient_unit: 'cl',
      },
      {
        ingredient_id: 'coca_id',
        ingredient_qty: '15',
        ingredient_unit: 'cl',
      },
      {
        ingredient_id: 'citron',
        ingredient_qty: '1',
        ingredient_unit: 'cl',
      },

    ],
  },
];

module.exports = Recettes;
