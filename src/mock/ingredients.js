const Ingredients = [
  {
    id: 'vodka_id',
    name: 'Vodka',
  },
  {
    id: 'wisky_id',
    name: 'Wisky',
  },
  {
    id: 'gin_id',
    name: 'Gin',
  },
  {
    id: 'rhum_id',
    name: 'Rhum',
  },
  {
    id: 'orange_id',
    name: 'Orange',
  },
  {
    id: 'coca_id',
    name: 'Coca',
  },
  {
    id: 'citronvert_id',
    name: 'Citron vert',
  },
  {
    id: 'citronjaune_id',
    name: 'Citron jaune',
  },
];

module.exports = Ingredients;
