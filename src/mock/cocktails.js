const Cocktails = [
  {
    id: 'cocktail-1',
    name: 'Wisky Coca',
    recettes: ['recette1'],
  },
  {
    id: 'cocktail-2',
    name: 'Vodka Orange',
    recettes: ['recette2', 'recette3'],
  },
  {
    id: 'cocktail-3',
    name: 'Long iceland',
    recettes: ['recette3'],
  },
];

module.exports = Cocktails;
